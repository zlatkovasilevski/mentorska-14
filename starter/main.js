//solution for exercise 1
function onloadFunction() {
  let element = document.getElementById("target");
  let positionInfo = element.getBoundingClientRect();
  let imgHeight = positionInfo.height;
  let imgWidth = positionInfo.width;

  let screenWidth = window.innerWidth;
  let screenHeight = window.innerHeight;

  let imgLeft = Math.floor(Math.random() * (screenWidth - imgWidth));
  let imgTop = Math.floor(Math.random() * (screenHeight - imgHeight));

  element.style.top = imgTop + "px";
  element.style.left = imgLeft + "px";
}
let clicks = 0;

//solution for exercise 2
document.getElementById("target").addEventListener("click", function (event) {
  //console.log(event);
  $("#exampleModal").modal("show");

  // Exercise 4

  let result = document.querySelector(".number_score");
  clicks++;
  result.innerText = clicks;

  //   Exercise 5

  localStorage.setItem("score", `${clicks}`);
});
window.addEventListener("load", () => {
  let myScore = localStorage.getItem("score");
  if (myScore) {
    window.addEventListener("load", function (e) {
      preventDefault(e);
      e.innerText = `${clicks}`;
    });
  }
});
//solution for exercise 3
document
  .getElementById("play-again")
  .addEventListener("click", function (event) {
    onloadFunction();
    $("#exampleModal").modal("hide");
  });

// exercise 6

let buttonReset = document.querySelector(".reset");

buttonReset.addEventListener("click", function () {
  clicks = 0;
  localStorage.setItem("score", clicks);
  onloadFunction();
});
